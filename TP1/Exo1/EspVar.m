function [ret] = EspVar (Vals, Probs)
  esperence = sum(Vals.*Probs);
  Variance = sum(power(Vals,2).*Probs)-power(esperence,2);
  ret = [esperence, Variance];
  
endfunction
