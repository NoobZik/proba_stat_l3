```
A(2,3) // Affiche la case 2,3 de la matrice A
size(A) // Affiche les dimensions de la matrice
classe(A) // Types de donénes
numel(A) // Nombre d'element dans la matrice.
A(1,end) // Affiche la valeur de la dernière colonne ligne 1.
A(end, 2) // Affiche la valeur de la derniere ligne colonne 2.
A(12,72) // Affiche une erreur car la dimension est 3x4.
```

```
A(1, 2) = 3 // Affecte 3 a la case 1.2.
A(1, end) = 78 // Affecte 78 a la derniere colonne premiere ligne.
A(4,2) = 52 // Erreur d'affectation a cause de la dimension
A' // Inverse d'une matrice.
sum(A) effectue la somme de chaque colonne.
cumsum(A) Effectue sucessivement la some de la ligne 1 a la ligne 2
```
