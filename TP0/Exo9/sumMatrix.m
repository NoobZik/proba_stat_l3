function [ret] = sumMatrix(A) 

  totTab = sum(A) ;
  temp = 0 ;
  
  for i = 1:length(totTab)
    temp = temp + totTab(i) ; 
  endfor
  ret = temp ;
endfunction
