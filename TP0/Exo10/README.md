## Question 2 ##

```
A + B // Effectue la somme de deux matrices
A + C // Erreur de dimension de la somme de deux matrices.
2 * D // Effectue la multiplication de la matrice D par 2
3*A-2*B //Effectue la multiplication des matrices et ensuite la différence
```
## Question 3 ##

```
A .* B // multiplication de deux matrice, de chaque indice avec une matrice de taille identique.
A .* C // Impossible car les matrices ne sont pas de tailles égales.
A .^ 2 // Eleve au carée chaque indice du tableau
B ./ A // Division de chaque indice de B par A
C ./ D // Impossible car matrice pas de meme taille.
```

## Question 4 ##
```
A * B // Impossible car longueur A != largeur B
A * C // Effectue la multiplication de la matrice A par
D * B // multiplication de la matrice D par B
D ^2 // Elevation au carrée de la matrice D
B ^2 // Marche pas car c'est pas une matrice carrée.

```

## Question 5 ##
```
exp(A) // Elevation de chaque case en exponentiel
sin(C) // Calcul le sinus de chaque case
```
