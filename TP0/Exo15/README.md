```
T > 0 // Operateur booléen pour chaque case vrai si c'est positive, faux si nul ou négatif
T < 5 | T == 5 // Pareil mais avec 5 ou plus.
find(T < 0) // Cherche tout les T < 0 vérifié et affiche les résultats
T(find(T < 0)) // Affiche les case du résultat de find(T<0). Dans l'exo, on affiche la case 1 et 5 (donc -1 et -4)
T(T<0) // Affiche les cases dont le résultat de T<0 est 1
```
