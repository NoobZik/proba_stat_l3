function [ret] = cube(x)
  ret = x * x * x;
end
