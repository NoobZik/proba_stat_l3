```
// Generation d'une table de multiplication 10*10
M = (1:10)'*(1:10)
// Replacement de la troisieme colone par la table de 12
M(:,3) = (1:10)*12
// Suppression d'une colonne
M(:,3) = []

```
