function [ret] = absolue(x)
  if (x < 0)
    ret = -x;
  else ret = x;
  endif
endfunction
