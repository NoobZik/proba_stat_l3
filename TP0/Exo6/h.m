function [ret] = h (x)
  if (x <= 1)
    ret = x + 1;
  elseif (1 < x && x < 3)
    ret = 2 * x;
  else
    ret = x * x;
  endif
endfunction
