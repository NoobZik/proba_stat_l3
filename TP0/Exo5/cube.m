cube_anh = @(x) x^3 ;
f_anh = @(x,y) 2*x + 3*y - 3 ;
g_anh = @(x) [cos(x), sin(x)] ;