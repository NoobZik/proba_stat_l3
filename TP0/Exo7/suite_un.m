function [ret] = suite_un(n) 
  U(1) = 1;
  for i = 1 : n;
    U(i+1) = sqrt(1+U(i));
  endfor
  ret = U;
endfunction
