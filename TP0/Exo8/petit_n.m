function [ret] = petit_n ()
  n = 1;
  while (log(log(n)) <= 2)
    n = n + 1;
  endwhile
  ret = n;
endfunction
