## Question 1 ##
```
zeros (2,6) genere une matrice 2*6 init à 0
zeros (5) genere une matrice carré 5 à 0
ones(2,6) pareil que 0 mais avec 1
eye(3) genere une matrice carrée de taille 3 avec la diagonal init a 1
```

## Question 2 ##
```
1:10 // Genere une matrice à une ligne de 1 à 10.
2:3:20 // Genere une matrice à une ligne de 2 à 20 avec un pas de 3.
1:.1:10 // Genere une matrice à une ligne de 1 à 10 avec un pas de .1
linspace(0,pi,10) // Genere des valeur de x à y avec la meme intervale de ces valeur
```
